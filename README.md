# Summary of Process
Once you have an SVG image of an icon you would like to add, you would clone the
[UA brand icons repository](https://bitbucket.org/uadigital/ua-brand-icons/src)
locally and add the SVG image files to `src/svg`. Then commit your changes and
submit a pull request. Once this happens, bitbucket pipelines will run commands
which takes all the SVG files in `src/svg` and converts them to font files with
icon class names that can be used throughout a site. Also as a fall back and
secondary reference, the process will minify the SVG images in `src/svg`, and
convert the SVG image files to PNG and place them into appropriate folders that
are named `png` and `svg` in the `dist` folder. As a site builder, our
recommendation is to reference UA brand icons via the CDN using brand icon class
names within your site. As a backup method you do have access to the icons via
PNG or SVG file formats in the `dist` folder as well.

## Steps for Introducing New Brand Icons into Font Files:
1. Create or recieve icon image/s in SVG format.
2. Name the SVG file/s what you would like the icon class name to be.
3. Clone the [UA brand icons repository](https://bitbucket.org/uadigital/ua-brand-icons/src) locally on your machine and switch to a new branch.
4. Add the new SVG file/s to the `src/svg` folder.
5. Commit the changes, and submit a pull request to merge changes in your branch to the master branch.
6. Bitbucket pipelines will run and execute the `gulp` command.
7. CDN will be updated with new updated minified version of ua-brand-icons.min.css.

## Advance Changes to CSS file

The `ua-brand-icons.css` and `ua-brand-icons.min.css` file is an auto generated
file from a template file you can see at `templates/ua-brand-icons-template.css`.
If you wish to make any modifications to the css file you will need to modify
the template `ua-brand-icons-template.css` file. The brand icon class names are
auto generated within the template css file and the output of that is what you
see in the `dist` folder as `ua-brand-icons.css` and `ua-brand-icons.min.css`.
Any changes made in the `ua-brand-icons.css` or `ua-brand-icons.min.css` will be
erased because bitbucket pipelines runs gulp tasks which deletes those files and
recreates them with the template.

## How to Save SVG in Illustrator
Save your file as SVG with the following settings:

- SVG Profiles: SVG 1.1
- Fonts Type: SVG
- Fonts Subsetting: None
- Options Image Location: Embed
### Advanced Options
- CSS Properties: Presentation Attributes
- Decimal Places: 1
- Encoding: UTF-8
- Output fewer elements: check
Leave the rest unchecked.

More in-depth information: http://www.adobe.com/inspire/2013/09/exporting-svg-illustrator.html
